import {Routes, RouterModule} from '@angular/router';
import {FormComponent} from './components/user/form/form.component';
import {ListComponent} from './components/user/list/list.component';
import {ActivationComponent} from './components/user/activation/activation.component';
import {HomeComponent} from './components/home/home.component';

const APP_ROUTES: Routes = [
  {path: 'home', component: HomeComponent},
  {path: 'usuarios/listar', component: ListComponent},
  {path: 'usuarios/agregar', component: FormComponent},
  {path: 'usuarios/editar/:id', component: FormComponent},
  {path: 'usuarios/activacion/:token', component: ActivationComponent},
  {path: '**', pathMatch: 'full', redirectTo: 'home'}
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);
