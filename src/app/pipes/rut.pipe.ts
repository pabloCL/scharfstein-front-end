import {Pipe, PipeTransform} from '@angular/core';
import {format} from 'rut.js';

@Pipe({
  name: 'rutPipe'
})
export class RutPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return format(value);
  }
}
