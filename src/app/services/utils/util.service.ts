import swal from 'sweetalert2';
import {Injectable} from '@angular/core';
import {FormGroup} from '@angular/forms';


@Injectable({
  providedIn: 'root'
})

export class UtilService {

  constructor() {
  }

  public static swalmsg(type: boolean, message?: string, action?: any): void {
    const msg = message ? message : type ? 'success' : 'Ocurrio un error inesperado';
    type ? swal.fire({type: 'success', title: 'Operación exitosa!', text: msg}).then(action) :
      swal.fire({type: 'error', title: 'Error!', text: msg}).then(action);
  }

  public static markAsDirty(formGroup: FormGroup): void {
    (Object as any).values(formGroup.controls).forEach(control => {
      control.controls ? this.markAsDirty(control) : control.markAsDirty();
    });
  }
}

