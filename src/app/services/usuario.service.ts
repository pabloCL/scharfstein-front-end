import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Usuario} from '../model/usuario';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  private url = `${environment.baseUrl}usuarios/`;
  private httpOptions = {headers: new HttpHeaders({'Content-type': 'application/json'})};

  constructor(private http: HttpClient) {
  }

  save(usuario: Usuario): Observable<any> {
    return this.http.post<any>(this.url, usuario, this.httpOptions);
  }

  findById(id: number): Observable<any> {
    return this.http.get<Usuario>(`${this.url}/${id}`, this.httpOptions);
  }

  findAll(): Observable<Usuario[]> {
    return this.http.get<Usuario[]>(this.url, this.httpOptions);
  }

  deleteById(id: number): Observable<any> {
    return this.http.delete<any>(`${this.url}${id}`, this.httpOptions);
  }

  activate(token: string): Observable<any> {
    return this.http.post<Usuario>(`${this.url}activate/${token}`, this.httpOptions);
  }

  update(usuario: Usuario): Observable<any> {
    return this.http.put<any>(this.url, usuario, this.httpOptions);
  }
}
