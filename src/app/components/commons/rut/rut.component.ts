import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {clean, format, validate} from 'rut.js';

@Component({
  selector: 'app-rut',
  templateUrl: './rut.component.html',
  styleUrls: ['./rut.component.css']
})
export class RutComponent implements OnInit {

  @Output() rutEmmiter: EventEmitter<string> = new EventEmitter<string>();
  @Output() validPropietario: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Input() tipo: number;
  rut = '';
  required = false;
  invalid = false;
  existingPropietario: boolean;

  ngOnInit(): void {
  }

  onChange(e): void {
    let rut = e.target.value;
    if (rut !== '') {
      rut = clean(rut);
      if (validate(rut)) {
        this.rut = format(rut);
        this.required = false;
        this.invalid = false;
        this.rutEmmiter.emit(this.rut);
      } else {
        this.invalid = true;
        this.required = false;
        this.existingPropietario = false;
      }
    } else {
      this.existingPropietario = false;
      this.required = true;
      this.invalid = false;
    }
  }

  checkValidity(): void {
    this.required = true;
    this.invalid = false;
  }

  // checkRut(rut: string) {
  //   rut = rut.substring(0, rut.length - 1);
  //   switch (this.tipo) {
  //     case 0:
  //       this.propietarioService.validarPropietario(rut).subscribe(data => {
  //         this.existingPropietario = data;
  //         this.validPropietario.emit(data);
  //       }, error => {
  //         console.log('error');
  //       });
  //       break;
  //   }
  // }

  setRequired(b: boolean): void {
    this.required = b;
  }
}
