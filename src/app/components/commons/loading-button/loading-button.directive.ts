import {Directive, ElementRef, Input, OnInit, Renderer2} from '@angular/core';

@Directive({
  selector: '[appLoadingButton]'
})
export class LoadingButtonDirective implements OnInit {

  private child: ElementRef;
  private texto: string;
  private oldClass: string;

  constructor(private element: ElementRef, private renderer: Renderer2) {

  }

  private _working: boolean;
  private admitChange = false;

  @Input('appLoadingButton')
  set working(value: boolean) {
    this._working = value;
    this.loading();
  }

  loading() {
    const child = this.element.nativeElement.getElementsByTagName('i')[0];
    if (this._working) {
      this.renderer.setAttribute(this.element.nativeElement, 'disabled', 'true');
      this.admitChange = true;
    }
    if (!this._working) {
      if (this.admitChange) {
        this.renderer.removeAttribute(this.element.nativeElement, 'disabled');
        child.className = this.oldClass;
        this.admitChange = false;
      }
    }
  }

  ngOnInit(): void {
    const son = this.element.nativeElement.getElementsByTagName('i')[0];
    if (son && son != undefined && son != null) {
      this.child = this.element.nativeElement.getElementsByTagName('i')[0];
      this.oldClass = son.className;
    } else {
      this.texto = this.renderer.createText(this.element.nativeElement.innerHTML);
      this.element.nativeElement.innerHTML = '';
      const addChild = this.renderer.createElement('i');
      this.renderer.appendChild(this.element.nativeElement, addChild);
      this.renderer.appendChild(this.element.nativeElement, this.texto);
    }
  }

}
