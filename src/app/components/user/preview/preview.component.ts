import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {Usuario} from '../../../model/usuario';
import {format} from 'rut.js';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {RutComponent} from '../../commons/rut/rut.component';
import {UsuarioService} from '../../../services/usuario.service';
import {UtilService} from '../../../services/utils/util.service';

@Component({
  selector: 'app-preview',
  templateUrl: './preview.component.html',
  styleUrls: ['./preview.component.css']
})
export class PreviewComponent implements OnInit {

  @Input() usuario: Usuario;
  nombre: string = null;
  apellido: string = null;
  correo: string = null;
  working = false;
  rut: string = null;
  @ViewChild('rutComponent') rutComponent: RutComponent;

  usuarioForm = this.formBuilder.group({
    nombre: [null, [Validators.required]],
    apellido: [null, [Validators.required]],
    correo: [null, [Validators.required, Validators.email]],
  });

  constructor(private formBuilder: FormBuilder,
              private usuarioService: UsuarioService,
              private activeModal: NgbActiveModal) {
  }

  ngOnInit(): void {
    if (this.usuario != null) {
      this.rut = format(this.usuario.rut + this.usuario.dv);
      this.nombre = this.usuario.nombre;
      this.apellido = this.usuario.apellido;
      this.correo = this.usuario.correo;
    }
  }

  editar(): void {
    if (this.usuarioForm.valid) {
      this.usuario.nombre = this.nombre;
      this.usuario.apellido = this.apellido;
      this.usuario.correo = this.correo;
      this.working = true;
      this.usuarioService.update(this.usuario).subscribe(data => {
        this.working = false;
        UtilService.swalmsg(true, data.mensaje, this.activeModal.close(data));
      }, error => {
        this.working = false;
        UtilService.swalmsg(false, error.error.mensaje);
      });
    } else {
      UtilService.markAsDirty(this.usuarioForm);
    }
  }
}
