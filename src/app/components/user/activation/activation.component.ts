import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {UsuarioService} from '../../../services/usuario.service';
import {Usuario} from '../../../model/usuario';
import {UtilService} from '../../../services/utils/util.service';
import {dashCaseToCamelCase} from '@angular/compiler/src/util';

@Component({
  selector: 'app-activation',
  templateUrl: './activation.component.html',
  styleUrls: ['./activation.component.css']
})
export class ActivationComponent implements OnInit {

  usuario: Usuario = null;
  error: string = null;
  token: string = null;

  constructor(private activatedRoute: ActivatedRoute,
              private usuarioService: UsuarioService) {
  }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((param) => {
      if (param.token) {
        this.token = param.token;
        this.activate(this.token);
      }
    });
  }

  private activate(token: string): void {
    if (token != null) {
      this.usuarioService.activate(token).subscribe(data => {
        this.usuario = data.usuario;
      }, error => {
        this.error = error.error.mensaje;
        this.usuario = error.error.usuario;
      });
    }
  }
}
