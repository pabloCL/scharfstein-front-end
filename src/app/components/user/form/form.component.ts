import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {Usuario} from '../../../model/usuario';
import {UsuarioService} from '../../../services/usuario.service';
import {RutComponent} from '../../commons/rut/rut.component';
import {Router} from '@angular/router';
import {clean, validate} from 'rut.js';
import {UtilService} from '../../../services/utils/util.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  nombre: string = null;
  apellido: string = null;
  rut: string = null;
  usuario: Usuario = new Usuario();
  working = false;

  @ViewChild('rutComponent') rutComponent: RutComponent;

  usuarioForm = this.formBuilder.group({
    nombre: [null, [Validators.required]],
    apellido: [null, [Validators.required]],
    correo: [null, [Validators.required, Validators.email]],
  });

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private usuarioService: UsuarioService) {
  }

  ngOnInit(): void {
  }

  guardar(): void {
    if (!validate(this.rut)) {
      this.rutComponent.setRequired(true);
    }
    if (this.usuarioForm.valid) {
      const rut = clean(this.rut);
      const cuerpo = rut.substring(0, rut.length - 1);
      const dv = rut.substring(rut.length - 1);
      this.usuario.rut = Number(cuerpo);
      this.usuario.dv = dv;
      this.working = true;
      this.usuarioService.save(this.usuario).subscribe(data => {
        this.working = false;
        UtilService.swalmsg(true, data.mensaje, this.router.navigate(['/usuarios/listar']));
      }, error => {
        this.working = false;
        UtilService.swalmsg(false, error.error.mensaje);
      });
    } else {
      UtilService.markAsDirty(this.usuarioForm);
    }
  }
}
