import {Component, OnInit} from '@angular/core';
import {Usuario} from '../../../model/usuario';
import {UsuarioService} from '../../../services/usuario.service';
import {PreviewComponent} from '../preview/preview.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
import {UtilService} from '../../../services/utils/util.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  usuarios: Usuario[] = [];

  constructor(private usuarioService: UsuarioService,
              private modalService: NgbModal) {
  }

  ngOnInit(): void {
    this.getUsuarios();
  }

  private getUsuarios(): void {
    this.usuarioService.findAll().subscribe(data => {
      console.log(data);
      this.usuarios = data;
    }, error => {
      console.log(error);
    });
  }

  editar(usuario: Usuario): void {
    const modalRef = this.modalService.open(PreviewComponent, {size: 'lg'});
    modalRef.componentInstance.usuario = usuario;
    modalRef.result.then((entry) => {
      if (entry) {
        console.log(entry);
      }
    });
  }

  delete(u: Usuario, i): void {
    Swal.fire({
      title: 'Atención!',
      text: '¿Está seguro de eliminar el usuario?',
      type: 'info',
      showCancelButton: true,
      reverseButtons: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar',
      cancelButtonText: 'No, cancelar',
    }).then(result => {
      if (result.value) {
        this.usuarioService.deleteById(u.id).subscribe(data => {
          UtilService.swalmsg(true, data.mensaje, this.usuarios.splice(i, 1));
        }, error => {
          UtilService.swalmsg(false, error.error.mensaje);
        });
      }
    });
  }
}
