export class Usuario {
  id: number;
  rut: number;
  dv: string;
  nombre: string;
  apellido: string;
  correo: string;
  estado: boolean;
  creadoEn: Date;
}
