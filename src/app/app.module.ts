import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {FormComponent} from './components/user/form/form.component';
import {APP_ROUTING} from './app.routes';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RutComponent} from './components/commons/rut/rut.component';
import {HttpClientModule} from '@angular/common/http';
import {ListComponent} from './components/user/list/list.component';
import {NavbarComponent} from './components/shared/navbar/navbar.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {RutPipe} from './pipes/rut.pipe';
import {PreviewComponent} from './components/user/preview/preview.component';
import {ActivationComponent} from './components/user/activation/activation.component';
import {HomeComponent} from './components/home/home.component';
import {ToastrModule} from 'ngx-toastr';
import { LoadingButtonDirective } from './components/commons/loading-button/loading-button.directive';

@NgModule({
  declarations: [
    AppComponent,
    FormComponent,
    RutComponent,
    ListComponent,
    NavbarComponent,
    RutPipe,
    PreviewComponent,
    ActivationComponent,
    HomeComponent,
    LoadingButtonDirective,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    APP_ROUTING,
    ReactiveFormsModule,
    FormsModule,
    NgbModule,
    ToastrModule.forRoot({
      timeOut: 10000,
      positionClass: 'toast-bottom-right',
    })
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [
    PreviewComponent
  ]
})
export class AppModule {
}
